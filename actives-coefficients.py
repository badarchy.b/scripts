import snowflake.connector
import json
import os

snowflake_credentials_load_path = '~/credentials/snowflake_credentials.json'

# Expanding the user's home directory symbol (~)
full_path = os.path.expanduser(snowflake_credentials_load_path)

with open(full_path, 'r') as file:
    snowflake_credentials = json.load(file)

connection = snowflake.connector.connect(
    user = snowflake_credentials['user'],
    password = snowflake_credentials['password'],
    account = snowflake_credentials['account'],
    database = snowflake_credentials['database'],
    role = snowflake_credentials['role']
    )
cursor = connection.cursor()

q = """
SELECT 
    monthly_data.monthly_uaps,
    monthly_data.date,
    monthly_data.market,
    quarterly_data.quarterly_uaps
FROM (
    SELECT 
        COUNT(DISTINCT aa.account_idt) AS monthly_uaps,
        CAST(DATE_TRUNC('month', aa.date) AS DATE) AS date,
        aa.market
    FROM (
        SELECT 
            c.account_idt,
            cb.date,
            dc.market
        FROM 
            DWH_DB.DWH.fact_casino_bets cb
        INNER JOIN 
            DWH_DB.DWH.dim_customers c ON c.account_idt = cb.account_idt
        INNER JOIN 
            DWH_DB.DWH.dim_countries dc ON dc.country_idt = c.country_idt
        WHERE 
            cb.date >= '2023-10-01' AND cb.date < '2024-01-01'
            AND c.type IN ('casino', 'sport')
        
        UNION ALL
        
        SELECT 
            c.account_idt,
            sb.date,
            dc.market
        FROM 
            DWH_DB.DWH.fact_sports_bets sb
        INNER JOIN 
            DWH_DB.DWH.dim_customers c ON c.account_idt = sb.account_idt
        INNER JOIN 
            DWH_DB.DWH.dim_countries dc ON dc.country_idt = c.country_idt
        WHERE 
            sb.date >= '2023-10-01' AND sb.date < '2024-01-01'
            AND c.type IN ('casino', 'sport')
    ) AS aa
    GROUP BY 
        aa.market, DATE_TRUNC('month', aa.date)
) AS monthly_data
LEFT JOIN (
    SELECT 
        COUNT(DISTINCT aa.account_idt) AS quarterly_uaps,
        aa.market
    FROM (
        SELECT 
            c.account_idt,
            cb.date,
            dc.market
        FROM 
            DWH_DB.DWH.fact_casino_bets cb
        INNER JOIN 
            DWH_DB.DWH.dim_customers c ON c.account_idt = cb.account_idt
        INNER JOIN 
            DWH_DB.DWH.dim_countries dc ON dc.country_idt = c.country_idt
        WHERE 
            cb.date >= '2023-10-01' AND cb.date < '2024-01-01'
            AND c.type IN ('casino', 'sport')
        
        UNION ALL
        
        SELECT 
            c.account_idt,
            sb.date,
            dc.market
        FROM 
            DWH_DB.DWH.fact_sports_bets sb
        INNER JOIN 
            DWH_DB.DWH.dim_customers c ON c.account_idt = sb.account_idt
        INNER JOIN 
            DWH_DB.DWH.dim_countries dc ON dc.country_idt = c.country_idt
        WHERE 
            sb.date >= '2023-10-01' AND sb.date < '2024-01-01'
            AND c.type IN ('casino', 'sport')
    ) AS aa
    GROUP BY 
        aa.market
) AS quarterly_data ON monthly_data.market = quarterly_data.market;

"""
actives = pd.read_sql(q, connection)
actives.to_csv('actives.csv', index=False)
country_to_iso = {
    "Australia": "AU",
    "Austria": "AT",
    "Azerbaijan": "AZ",
    "Brazil": "BR",
    "Canada": "CA",
    "Chile": "CL",
    "Czech Republic": "CZ",
    "Denmark": "DK",
    "Estonia": "EE",
    "Finland": "FI",
    "Germany": "DE",
    "Hungary": "HU",
    "India": "IN",
    "Ireland": "IE",
    "Italy": "IT",
    "Kazakhstan": "KZ",
    "New Zealand": "NZ",
    "Norway": "NO",
    "Peru": "PE",
    "Poland": "PL",
    "Romania": "RO",
    "Slovakia": "SK",
    "Slovenia": "SI",
    "Sweden": "SE",
    "Switzerland": "CH",
    "Thailand": "TH"
}


actives['DATE'] = actives['DATE'].astype('str')
actives_grouped = actives.groupby('MARKET').agg({
    'MONTHLY_UAPS':'sum',
    'QUARTERLY_UAPS':'max'

})

actives_grouped['Actives Coefficients'] = actives_grouped['QUARTERLY_UAPS']/(actives_grouped['MONTHLY_UAPS']) #* 100
actives_grouped.reset_index(inplace=True)
actives_grouped = actives_grouped[['MARKET','Actives Coefficients']]
actives_grouped = actives_grouped.rename(columns={'MARKET':'Market'})
actives_grouped['Country'] = actives_grouped['Market'].replace(country_to_iso)
actives_grouped = actives_grouped[['Country','Actives Coefficients','Market']] 
actives_grouped.to_clipboard(index=False)