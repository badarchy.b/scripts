DECLARE @from DATE = '2023-06-01'
DECLARE @to   DATE = '2023-09-30'

DROP TABLE IF EXISTS #promo
DROP TABLE IF EXISTS #promo_trx

SELECT T.domain,
       T.account_id,
       1 AS get_promo
INTO #promo

FROM replica_api_bonus_source T with (nolock)

         JOIN replica_api_bonus_source TT with (nolock)
              ON T.bonus_id = TT.parent_id
                  AND T.account_id = TT.account_id
                  AND T.domain = TT.domain

         JOIN Fact_BonusReleased R with (nolock)
              ON R.account_id = T.account_id
                  AND R.domain = T.domain
                  AND R.bonus_id = TT.bonus_id
                  AND R.type = 'bonus_deposit'

WHERE (T.name LIKE '%summer%' OR T.name LIKE '%voyage%' OR T.name LIKE '%SP2023%')
  AND T.created >= '2023-07-01'
  AND T.type = 'freespin'

GROUP BY T.domain,
         T.account_id

UNION

SELECT T.domain,
       T.account_id,
       1 AS get_promo

FROM replica_api_bonus T with (nolock)

         JOIN replica_api_bonus TT with (nolock)
              ON T.bonus_id = TT.parent_id
                  AND T.account_id = TT.account_id
                  AND T.domain = TT.domain

         JOIN Fact_BonusReleased R with (nolock)
              ON R.account_id = T.account_id
                  AND R.domain = T.domain
                  AND R.bonus_id = TT.bonus_id
                  AND R.type = 'bonus_deposit'

WHERE (T.name LIKE '%summer%' OR T.name LIKE '%voyage%' OR T.name LIKE '%SP2023%')
  AND T.created >= '2023-07-01'
  AND T.type = 'freespin'

GROUP BY T.domain,
         T.account_id

UNION

SELECT T.domain,
       T.account_id,
       1 AS get_promo

FROM Fact_BonusTransactions T with (nolock)

         JOIN Fact_BonusReleased R with (nolock)
              ON R.account_id = T.account_id
                  AND R.domain = T.domain
                  AND R.bonus_id = T.bonus_id
                  AND R.type = 'bonus_deposit'

WHERE (T.name LIKE '%summer%' OR T.name LIKE '%voyage%' OR T.name LIKE '%SP2023%')
  AND T.created >= '2023-07-01'

GROUP BY T.domain,
         T.account_id

-- promo trx

SELECT T.domain,
       T.account_id,
       1                                        AS get_promo,
       CAST(T.created AS DATE)                  AS period_,
       COUNT(DISTINCT R.account_transaction_id) AS trx_bonus

INTO #promo_trx

FROM replica_api_bonus_source T with (nolock)

         JOIN replica_api_bonus_source TT with (nolock)
              ON T.bonus_id = TT.parent_id
                  AND T.account_id = TT.account_id
                  AND T.domain = TT.domain

         JOIN Fact_BonusReleased R with (nolock)
              ON R.account_id = T.account_id
                  AND R.domain = T.domain
                  AND R.bonus_id = TT.bonus_id
                  AND R.type = 'bonus_deposit'

WHERE (T.name LIKE '%summer%' OR T.name LIKE '%voyage%' OR T.name LIKE '%SP2023%')
  AND T.created >= '2023-07-01'
  AND T.type = 'freespin'

GROUP BY T.domain,
         T.account_id,
         CAST(T.created AS DATE)

UNION

SELECT T.domain,
       T.account_id,
       1                                        AS get_promo,
       CAST(T.created AS DATE)                  AS period_,
       COUNT(DISTINCT R.account_transaction_id) AS trx_bonus

FROM replica_api_bonus T with (nolock)

         JOIN replica_api_bonus TT with (nolock)
              ON T.bonus_id = TT.parent_id
                  AND T.account_id = TT.account_id
                  AND T.domain = TT.domain

         JOIN Fact_BonusReleased R with (nolock)
              ON R.account_id = T.account_id
                  AND R.domain = T.domain
                  AND R.bonus_id = TT.bonus_id
                  AND R.type = 'bonus_deposit'

WHERE (T.name LIKE '%summer%' OR T.name LIKE '%voyage%' OR T.name LIKE '%SP2023%')
  AND T.created >= '2023-07-01'
  AND T.type = 'freespin'

GROUP BY T.domain,
         T.account_id,
         CAST(T.created AS DATE)

UNION

SELECT T.domain,
       T.account_id,
       1                                        AS get_promo,
       CAST(T.created AS DATE)                  AS period_,
       COUNT(DISTINCT R.account_transaction_id) AS trx_bonus

FROM Fact_BonusTransactions T with (nolock)

         JOIN Fact_BonusReleased R with (nolock)
              ON R.account_id = T.account_id
                  AND R.domain = T.domain
                  AND R.bonus_id = T.bonus_id
                  AND R.type = 'bonus_deposit'

WHERE (T.name LIKE '%summer%' OR T.name LIKE '%voyage%' OR T.name LIKE '%SP2023%')
  AND T.created >= '2023-07-01'

GROUP BY T.domain,
         T.account_id,
         CAST(T.created AS DATE)

-- total


SELECT ReportingDate,
       business,
       brand,
       market,
       traffic,
       VIP_level,

       -- no promo
       COUNT(DISTINCT
             CASE WHEN real_bet > 0 AND get_promo = 0 THEN CONCAT(account_id, domain) ELSE NULL END)          AS active_no_promo,
       COUNT(DISTINCT CASE
                          WHEN DepositAmount_EUR > 0 AND get_promo = 0 THEN CONCAT(account_id, domain)
                          ELSE NULL END)                                                                      AS depositors_no_promo,
       SUM(CASE WHEN get_promo = 0 THEN real_bet ELSE 0 END)                                                  AS real_bet_no_promo,
       SUM(CASE WHEN get_promo = 0 THEN GGR_total ELSE 0 END)                                                 AS GGR_total_no_promo,
       SUM(CASE WHEN get_promo = 0 THEN NGR ELSE 0 END)                                                       AS NGR_no_promo,
       SUM(CASE WHEN get_promo = 0 THEN Turnover_total ELSE 0 END)                                            AS Turnover_total_no_promo,
       SUM(CASE WHEN get_promo = 0 THEN DepositAmount_EUR ELSE 0 END)                                         AS Deposits_no_promo,
       SUM(CASE WHEN get_promo = 0 THEN DepositCount ELSE 0 END)                                              AS DepositCount_no_promo,
       SUM(CASE WHEN get_promo = 0 THEN WithdrawAmount_EUR ELSE 0 END)                                        AS Withdraw_no_promo,

       -- promo
       COUNT(DISTINCT CASE
                          WHEN real_bet > 0 AND get_promo = 1 THEN CONCAT(account_id, domain)
                          ELSE NULL END)                                                                      AS active_promo,
       COUNT(DISTINCT CASE
                          WHEN DepositAmount_EUR > 0 AND get_promo = 1 THEN CONCAT(account_id, domain)
                          ELSE NULL END)                                                                      AS depositors_promo,
       SUM(CASE WHEN get_promo = 1 THEN real_bet ELSE 0 END)                                                  AS real_bet_promo,
       SUM(CASE WHEN get_promo = 1 THEN GGR_total ELSE 0 END)                                                 AS GGR_total_promo,
       SUM(CASE WHEN get_promo = 1 THEN NGR ELSE 0 END)                                                       AS NGR_promo,
       SUM(CASE WHEN get_promo = 1 THEN Turnover_total ELSE 0 END)                                            AS Turnover_total_promo,
       SUM(CASE WHEN get_promo = 1 THEN DepositAmount_EUR ELSE 0 END)                                         AS Deposits_promo,
       SUM(CASE WHEN get_promo = 1 THEN DepositCount ELSE 0 END)                                              AS DepositCount_promo,
       SUM(CASE WHEN get_promo = 1 THEN WithdrawAmount_EUR ELSE 0 END)                                        AS Withdraw_promo,
       SUM(CASE WHEN get_promo = 1 AND business = 'B2C' THEN trx_bonus ELSE 0 END)                            AS trx_bonus_promo_B2C,
       SUM(CASE WHEN get_promo = 1 AND business = 'B2B' THEN trx_bonus ELSE 0 END)                            AS trx_bonus_promo_B2B

FROM (SELECT B.business,
             C.brand,
             A.account_id,
             A.domain,
             ReportingDate,
             C.affiliate_id,
             C.traffic,
             C.market,
             ISNULL(K.get_promo, 0)                                                                           AS get_promo,
             ISNULL(V.level, 1)                                                                               AS VIP_level,
             SUM(TurnoverCasino_EUR + TurnoverSport_EUR)                                                      AS real_bet,
             SUM(GGRCasino_EUR + GGRSport_EUR + bonus_GGRCasino_EUR + bonus_GGRSport_EUR)                     AS GGR_total,
             SUM(NGR)                                                                                         AS NGR,
             SUM(TurnoverCasino_EUR + TurnoverSport_EUR + bonus_TurnoverCasino_EUR +
                 bonus_TurnoverSport_EUR)                                                                     AS Turnover_total,
             SUM(DepositAmount_EUR)                                                                           AS DepositAmount_EUR,
             SUM(DepositCount)                                                                                AS DepositCount,
             SUM(WithdrawAmount_EUR)                                                                          AS WithdrawAmount_EUR,
             SUM(ISNULL(T.trx_bonus, 0))                                                                      AS trx_bonus

      FROM Fact_CustomerDailyActivity A WITH (NOLOCK)

               OUTER APPLY (SELECT *
                            FROM #promo
                            WHERE account_id = A.account_id
                              AND domain = A.domain) K

               OUTER APPLY (SELECT *
                            FROM Dim_Customers WITH (NOLOCK)
                            WHERE account_id = A.account_id
                              AND domain = A.domain) C

               OUTER APPLY (SELECT TOP 1 level
                            FROM Fact_LoyaltyLevel WITH (NOLOCK)
                            WHERE account_id = A.account_id
                              AND domain = A.domain
                            ORDER BY date DESC) V

               OUTER APPLY (SELECT *
                            FROM Dim_Brands WITH (NOLOCK)
                            WHERE entity_id = C.entity_id
                              AND domain = C.domain) B

               OUTER APPLY (SELECT TOP 1 *
                            FROM #promo_trx
                            WHERE account_id = A.account_id
                              AND domain = A.domain
                              AND ReportingDate = period_) T

      WHERE ReportingDate >= @from
        AND ReportingDate < DATEADD(dd, 1, @to)
        AND C.type IN ('casino', 'sport')
      -- AND ISNULL(K.get_promo,0) = 1

      GROUP BY B.business,
               C.brand,
               A.account_id,
               A.domain,
               ReportingDate,
               C.affiliate_id,
               C.traffic,
               ISNULL(K.get_promo, 0),
               ISNULL(V.level, 1),
               C.market) K

GROUP BY business,
         brand,
         VIP_level,
         ReportingDate,
         market,
         traffic


-- OUTER APPLY (SELECT * FROM Dim_AffiliateType WITH(NOLOCK) WHERE affiliate_id = K.affiliate_id ) J
