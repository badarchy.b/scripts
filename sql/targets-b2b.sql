declare @start as  DATE ='2022-07-01'
declare @finish as DATE ='2024-12-31'

SELECT k.mm                                                                                  Month_,
       br.business,
       k.b2b_partner,
       case
           when k.country in
                ('DE', 'IT', 'FI', 'SE', 'PL', 'NO', 'AT', 'IE', 'HU', 'CA', 'CZ', 'BR', 'CH', 'IN', 'NZ', 'JP', 'TH',
                 'AU', 'EE', 'TR', 'CL', 'PE', 'SK', 'SI', 'KZ', 'AZ', 'RO')
               then k.country
           when k.country = 'FR' then 'ROW1'
           when k.country = 'GR' then 'ROW2'
           when k.country = 'ES' then 'ROW3'
           when k.country = 'PT' then 'ROW5'
           when k.country = 'DK' and br.License = 'DGA' then k.country
           when k.country in ('KW', 'BH', 'QA', 'AE', 'SA', 'LB', 'JO', 'OM', 'IQ', 'YE', 'EG') then 'ME'
           else 'ROW' end                                                                    country,
       -- br.business,
       SUM(reg)                                                                         AS   reg,
       SUM(ftd)                                                                         AS   ftd,
       SUM(ftd) - SUM(ftd_sport)                                                             ftd_casino,
       SUM(ftd_sport)                                                                        ftd_sport,
       SUM(Activess)                                                                    AS   Activess,
       SUM(Activess) - SUM(Activess_sport)                                                   Activess_casino,
       SUM(Activess_sport)                                                                   Activess_sport,
       SUM(Activess) - SUM(ftd)                                                         AS   Retained,
       SUM(Activess) - SUM(Activess_sport) - (SUM(ftd) - SUM(ftd_sport))                     Retained_casino,
       SUM(Activess_sport) - SUM(ftd_sport)                                                  Retained_sport,
       ISNULL((SUM(Activess) - SUM(ftd)) / (nullif(1.00 * SUM(Activess), 0)), 0)        AS   per_Retained,
       SUM(Deposits)                                                                    AS   Deposits,
       SUM(Deposits) - SUM(withdraw)                                                    AS   Net_cash,
       SUM(GGRCasino_EUR)                                                               AS   GGRCasinoTotal_EUR,
       SUM(GGRSport_EUR)                                                                AS   GGRSportTotal_EUR,
       SUM(NGR)                                                                              NGR,
       ISNULL((SUM(NGR)) / (nullif(1.00 * SUM(Activess), 0)), 0)                             ARPU,
       ISNULL((SUM(NGR_FTD)) / (nullif(1.00 * SUM(ftd), 0)), 0)                              ARPU_FTD,
       ISNULL((SUM(NGR) - SUM(NGR_FTD)) / (nullif(1.00 * (SUM(Activess) - SUM(ftd)), 0)), 0) ARPU_old,
       ISNULL(SUM(Deposits_FTD), 0)                                                     AS   Deposits_FTD,
       ISNULL(SUM(Deposits_FTD) - SUM(withdraw_FTD), 0)                                 AS   Net_Cash_FTD,
       ISNULL(SUM(Deposits) - SUM(Deposits_FTD), 0)                                     AS   Deposits_old,
       ISNULL(SUM(Deposits) - SUM(Deposits_FTD) - SUM(withdraw) + SUM(withdraw_FTD), 0) AS   net_cash_old,
       sum(NGR_FTD)                                                                          NGR_FTD,
       SUM(NGR) - sum(NGR_FTD)                                                               NGR_old,
       SUM(Actives_VIP)                                                                      Actives_VIP,
       SUM(Actives_VIP) - SUM(FTD_VIP)                                                       retained_VIP,
       SUM(dep_VIP)                                                                          Deposits_VIP,
       SUM(net_VIP)                                                                          net_cash_VIP,
       SUM(NGR_VIP)                                                                          NGR_VIP,
       sum(ftd_aff)                                                                          ftd_aff,
       sum(ftd) - sum(ftd_aff)                                                               ftd_non_aff,
       SUM(depositors)                                                                       depositors,
       SUM(ret_depositors)                                                                   ret_depositors,
       SUM(old_users_ret)                                                                    old_users_ret,
       SUM(Retained_PrM)                                                                     Retained_PrM,
       SUM(Retained_Bef_PrM)                                                                 Retained_Bef_PrM
FROM (SELECT 1                  number,
             C.brand,
             BR.b2b_partner,
             C.domain,
             C.country_name,
             C.country,
             eomonth(C.created) mm,
             COUNT(*)           reg,
             0                  ftd,
             0                  ftd_sport,
             0                  ftd_aff,
             0                  Deposits,
             0                  Depositsors,
             0                  Deposits_FTD,
             0 AS               withdraw,
             0                  withdraw_FTD,
             0                  Activess,
             0                  Activess_sport,
             -- 0 net_cash,
             0 AS               GGRCasino_EUR,
             0 AS               bonus_GGRCasino_EUR,
             0 AS               GGRSport_EUR,
             0 AS               bonus_GGRSport_EUR,
             0 AS               DepositCount,
             0                  NGR,
             0                  NGR_FTD,
             0                  Actives_VIP,
             0                  FTD_VIP,
             0                  dep_VIP,
             0                  net_VIP,
             0                  NGR_VIP,
             0                  depositors,
             0                  ret_depositors,
             0                  old_users_ret,
             0                  Retained_PrM,
             0                  Retained_Bef_PrM
      FROM [dbo].[Dim_Customers] C with (nolock)
               JOIN [dbo].[Dim_Brands] BR with (nolock) ON BR.entity_id = C.entity_id AND BR.domain = C.domain
      WHERE type IN ('casino', 'sport')
        AND CAST(C.created as date) BETWEEN @start AND @finish
      GROUP BY C.domain, C.brand, C.country_name,
               C.country, BR.b2b_partner, eomonth(C.created)
      UNION
      SELECT 1                                                                                   number,
             C.brand,
             BR.b2b_partner,
             C.domain,
             C.country_name,
             C.country,
             eomonth(C.first_deposit_date)                                                       mm,
             0                                                                                   reg,
             COUNT(*)                                                                            ftd,
             COUNT(CASE WHEN C.product_preference IN ('mix', 'pure_sport') THEN 1 ELSE NULL END) ftd_sport,
             COUNT(CASE WHEN C.traffic = 'affiliate' THEN 1 ELSE NULL END)                       ftd_aff,
             0                                                                                   Deposits,
             0                                                                                   Depositsors,
             0                                                                                   Deposits_FTD,
             0 AS                                                                                withdraw,
             0                                                                                   withdraw_FTD,
             0                                                                                   Activess,
             0                                                                                   Activess_sport,
             -- 0 net_cash,
             0 AS                                                                                GGRCasino_EUR,
             0 AS                                                                                bonus_GGRCasino_EUR,
             0 AS                                                                                GGRSport_EUR,
             0 AS                                                                                bonus_GGRSport_EUR,
             0 AS                                                                                DepositCount,
             0                                                                                   NGR,
             0                                                                                   NGR_FTD,
             0                                                                                   Actives_VIP,
             0                                                                                   FTD_VIP,
             0                                                                                   dep_VIP,
             0                                                                                   net_VIP,
             0                                                                                   NGR_VIP,
             0                                                                                   depositors,
             0                                                                                   ret_depositors,
             0                                                                                   old_users_ret,
             0                                                                                   Retained_PrM,
             0                                                                                   Retained_Bef_PrM
      FROM [dbo].[Dim_Customers] C with (nolock)
               JOIN [dbo].[Dim_Brands] BR with (nolock) ON BR.entity_id = C.entity_id AND BR.domain = C.domain
      WHERE type IN ('casino', 'sport')
        AND C.first_deposit_date BETWEEN @start AND @finish
      GROUP BY C.brand, C.domain, C.country_name,
               C.country, BR.b2b_partner, eomonth(C.first_deposit_date)
      UNION
      SELECT 1                                                                                                  number,
             C.brand,
             BR.b2b_partner,
             C.domain,
             C.country_name,
             C.country,
             eomonth(ReportingDate)                                                                             mm,
             0                                                                                                  reg,
             0                                                                                                  ftd,
             0                                                                                                  ftd_sport,
             0                                                                                                  ftd_aff,
             SUM(DepositAmount_EUR)                                                                             Deposits,
             COUNT(DISTINCT CASE
                                WHEN CC.DepositAmount_EUR > 0 THEN CC.account_id
                                ELSE NULL END)                                                                  Depositsors,
             SUM(CASE
                     WHEN eomonth(C.first_deposit_date) = eomonth(ReportingDate) THEN CC.DepositAmount_EUR
                     ELSE 0 END)                                                                                Deposits_FTD,
             SUM(CC.WithdrawAmount_EUR) AS                                                                      withdraw,
             SUM(CASE
                     WHEN eomonth(C.first_deposit_date) = eomonth(ReportingDate) THEN CC.WithdrawAmount_EUR
                     ELSE 0 END)                                                                                withdraw_FTD,
             COUNT(DISTINCT CASE
                                WHEN CC.TurnoverCasino_EUR + CC.TurnoverSport_EUR > 0 THEN CC.account_id
                                ELSE NULL END)                                                                  Activess,
             COUNT(DISTINCT CASE
                                WHEN CC.TurnoverSport_EUR > 0 THEN CC.account_id
                                ELSE NULL END)                                                                  Activess_sport,
             -- COUNT(DISTINCT CASE WHEN CC.DepositAmount_EUR>0 OR WithdrawAmount_EUR>0 THEN CC.account_id ELSE NULL END) net_cash,
             SUM(GGRCasino_EUR)         AS                                                                      GGRCasino_EUR,
             SUM(bonus_GGRCasino_EUR)   AS                                                                      bonus_GGRCasino_EUR,
             SUM(GGRSport_EUR)          AS                                                                      GGRSport_EUR,
             SUM(bonus_GGRSport_EUR)    AS                                                                      bonus_GGRSport_EUR,
             SUM(DepositCount)                                                                                  DepositCount,
             SUM(NGR)                                                                                           NGR,
             SUM(CASE WHEN eomonth(C.first_deposit_date) = eomonth(ReportingDate) THEN NGR ELSE 0 END)          NGR_FTD,
             COUNT(DISTINCT CASE
                                WHEN CC.TurnoverCasino_EUR + CC.TurnoverSport_EUR > 0 and
                                     isnull(L.real_level_vip, 1) > 2 THEN CC.account_id
                                ELSE NULL END)                                                                  Actives_VIP,
             COUNT(DISTINCT CASE
                                WHEN eomonth(C.first_deposit_date) = eomonth(ReportingDate) and
                                     isnull(L.real_level_vip, 1) > 2 THEN CC.account_id
                                ELSE NULL END)                                                                  FTD_VIP,
             SUM(CASE WHEN isnull(L.real_level_vip, 1) > 2 THEN CC.DepositAmount_EUR ELSE 0 END)                dep_VIP,
             SUM(CASE
                     WHEN isnull(L.real_level_vip, 1) > 2 THEN CC.DepositAmount_EUR - CC.WithdrawAmount_EUR
                     ELSE 0 END)                                                                                net_VIP,
             SUM(CASE WHEN isnull(L.real_level_vip, 1) > 2 THEN CC.NGR ELSE 0 END)                              NGR_VIP,
             COUNT(DISTINCT CASE
                                WHEN CC.DepositCount > 0 THEN CC.account_id
                                ELSE NULL END)                                                                  depositors,
             COUNT(DISTINCT CASE
                                WHEN CC.DepositCount > 0 and eomonth(C.first_deposit_date) < eomonth(ReportingDate)
                                    THEN CC.account_id
                                ELSE NULL END)                                                                  ret_depositors,
             COUNT(DISTINCT CASE
                                WHEN CC.DepositCount > 0 and eomonth(D.LastDep) = eomonth(ReportingDate, -1) and
                                     eomonth(C.first_deposit_date) < eomonth(ReportingDate, -1) THEN CC.account_id
                                ELSE NULL END)                                                                  old_users_ret,
             COUNT(DISTINCT CASE
                                WHEN CC.TurnoverCasino_EUR + CC.TurnoverSport_EUR > 0 and
                                     eomonth(C.first_deposit_date) = eomonth(ReportingDate, -1) THEN CC.account_id
                                ELSE NULL END)                                                                  Retained_PrM,
             COUNT(DISTINCT CASE
                                WHEN CC.TurnoverCasino_EUR + CC.TurnoverSport_EUR > 0 and
                                     eomonth(C.first_deposit_date) < eomonth(ReportingDate, -1) THEN CC.account_id
                                ELSE NULL END)                                                                  Retained_Bef_PrM
      FROM Fact_CustomerDailyActivity CC with (nolock)
               JOIN [dbo].[Dim_Customers] C with (nolock) ON C.account_id = CC.account_id AND CC.domain = C.domain
               JOIN [dbo].[Dim_Brands] BR with (nolock) ON BR.entity_id = C.entity_id AND BR.domain = C.domain
               outer apply (SELECT top 1 CASE
                                             WHEN (L.level < 2 OR L.level IS NULL) THEN 1
                                             WHEN (L.level < 3 AND L.level >= 2) THEN 2
                                             WHEN (L.level < 4 AND L.level >= 3) THEN 3
                                             WHEN (L.level < 5 AND L.level >= 4) THEN 4
                                             WHEN L.level = 5 THEN 5
                                             END real_level_vip
                            FROM Fact_LoyaltyLevel L with (nolock)
                            where L.account_id = CC.account_id
                              and L.date <= eomonth(ReportingDate)
                              AND L.domain = CC.domain
                            order by L.date desc) L
               outer apply (select max(A.ReportingDate) LastDep
                            from Fact_CustomerDailyActivity A with (nolock)
                            where A.account_id = CC.account_id
                              and A.domain = CC.domain
                              and eomonth(A.ReportingDate) < eomonth(CC.ReportingDate)
                              and A.DepositCount > 0) D
      WHERE C.type IN ('casino', 'sport')
        AND CC.ReportingDate BETWEEN @start AND @finish
      GROUP BY C.brand, C.domain, C.country_name,
               C.country, BR.b2b_partner, eomonth(ReportingDate)) k
         JOIN Dim_Brands br ON br.name = k.brand AND br.domain = k.domain
where br.business in ('b2b')
GROUP BY k.mm, br.business,
         k.b2b_partner,
         case
             when k.country in
                  ('DE', 'IT', 'FI', 'SE', 'PL', 'NO', 'AT', 'IE', 'HU', 'CA', 'CZ', 'BR', 'CH', 'IN', 'NZ', 'JP', 'TH',
                   'AU', 'EE', 'TR', 'CL', 'PE', 'SK', 'SI', 'KZ', 'AZ', 'RO')
                 then k.country
             when k.country = 'FR' then 'ROW1'
             when k.country = 'GR' then 'ROW2'
             when k.country = 'ES' then 'ROW3'
             when k.country = 'PT' then 'ROW5'
             when k.country = 'DK' and br.License = 'DGA' then k.country
             when k.country in ('KW', 'BH', 'QA', 'AE', 'SA', 'LB', 'JO', 'OM', 'IQ', 'YE', 'EG') then 'ME'
             else 'ROW' end