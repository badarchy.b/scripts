WITH MVL AS(
          SELECT  ACCOUNT_IDT
                  ,DATE
                  ,LEVEL
                  ,ROW_NUMBER() OVER (PARTITION BY ACCOUNT_IDT ORDER BY LEVEL DESC,DATE DESC) AS MAX_LEVEL
                  ,ROW_NUMBER() OVER (PARTITION BY ACCOUNT_IDT ORDER BY DATE DESC) AS LAST_LEVEL
          FROM DWH.FACT_LOYALTY_LEVEL
          )
    SELECT
        k.mm AS Month_,
        br.business,
        k.License,
        k.affiliate_program,
        k.ENTITY_NAME,
        k.traffic,
        CASE
            WHEN k.country_iso IN ('DE','IT','FI','SE','PL','NO','AT','IE','HU','CA','CZ','BR','CH','IN','NZ','JP','TH','AU','EE','TR','CL','PE','SK','SI','KZ','AZ') THEN k.country_iso
            WHEN k.country_name IN ('ROW1','ROW2','ROW3') THEN k.country_name
            WHEN k.country_iso='DK' AND k.License = 'DGA' THEN k.country_iso
            WHEN k.country_iso IN ('KW','BH','QA','AE','SA','LB','JO','OM','IQ','YE','EG') THEN 'ME'
            ELSE 'ROW'
        END AS country,
        SUM(reg) AS reg,
        SUM(ftd) AS ftd,
        SUM(ftd)-SUM(ftd_sport) AS ftd_casino,
        SUM(ftd_sport) AS ftd_sport,
        SUM(Actives) AS Actives,
        SUM(Actives)-SUM(Actives_sport) AS Actives_casino,
        SUM(Actives_sport) AS Actives_sport,
        SUM(Actives) - SUM(ftd) AS Retained,
        SUM(Actives)-SUM(Actives_sport)-(SUM(ftd)-SUM(ftd_sport)) AS Retained_casino,
        SUM(Actives_sport)-SUM(ftd_sport) AS Retained_sport,
        NVL ( (SUM(Actives) - SUM(ftd)) / (NULLIF(1.00*SUM(Actives),0)), 0) AS per_Retained,
        SUM(deposit) AS deposit,
        SUM(deposit) - SUM(withdraw) AS Net_cash,
        SUM(GGRCasino_EUR) AS GGRCasinoTotal_EUR,
        SUM(GGRSport_EUR) AS GGRSportTotal_EUR,
        SUM(NGR) AS NGR,
        NVL ( (SUM(NGR)) / (NULLIF(1.00*SUM(Actives),0)), 0) AS ARPU,
        NVL ( (SUM(NGR_FTD)) / (NULLIF(1.00*SUM(ftd),0)), 0) AS ARPU_FTD,
        NVL ( (SUM(NGR)-SUM(NGR_FTD)) / (NULLIF(1.00*(SUM(Actives) - SUM(ftd)),0)), 0) AS ARPU_old,
        NVL(SUM(deposit_FTD),0) AS deposit_FTD,
        NVL(SUM(deposit_FTD) - SUM(withdraw_FTD),0) AS Net_Cash_FTD,
        NVL(SUM(deposit) - SUM(deposit_FTD),0) AS deposit_old,
        NVL(SUM(deposit) - SUM(deposit_FTD) - SUM(withdraw) + SUM(withdraw_FTD),0) AS net_cash_old,
        SUM(NGR_FTD) AS NGR_FTD,
        SUM(NGR) - SUM(NGR_FTD) AS NGR_old,
        SUM(active_VIP) AS active_VIP,
        SUM(active_VIP)-SUM(FTD_VIP) AS retained_VIP,
        SUM(dep_VIP) AS deposit_VIP,
        SUM(net_VIP) AS net_cash_VIP,
        SUM(NGR_VIP) AS NGR_VIP,
        SUM(TO_Casino) AS TO_Casino,
        SUM(TO_Sport) AS TO_Sport,
        SUM(ActiveDays) AS ActiveDays,
        SUM(Retained_PrM) AS Retained_PrM,
        SUM(Retained_Bef_PrM) AS Retained_Bef_PrM,
        SUM(depositors) AS depositors,
        SUM(DepRet_PrM) AS DepRet_PrM,
        SUM(DepRet_Bef_PrM) AS DepRet_Bef_PrM,
        SUM(old_depositors) AS old_depositors
    FROM (
SELECT
            BR.ENTITY_NAME,
            BR.License,
            BR.affiliate_program,
            C.domain,
            DC.COUNTRY_ISO,
            DC.COUNTRY_NAME,
            CASE WHEN LOWER(C.traffic)='affiliate' THEN 'affiliate' ELSE 'media' END AS traffic,
            LAST_DAY(C.created) AS mm,
            COUNT(*) AS reg,
            0 AS ftd,
            0 AS ftd_sport,
            0 AS deposit,
            0 AS depositors,
            0 AS deposit_FTD,
            0 AS withdraw,
            0 AS withdraw_FTD,
            0 AS Actives,
            0 AS Actives_sport,
            0 AS GGRCasino_EUR,
            0 AS bonus_GGRCasino_EUR,
            0 AS GGRSport_EUR,
            0 AS bonus_GGRSport_EUR,
            0 AS DepositCount,
            0 AS NGR,
            0 AS NGR_FTD,
            0 AS active_VIP,
            0 AS FTD_VIP,
            0 AS dep_VIP,
            0 AS net_VIP,
            0 AS NGR_VIP,
            0 AS TO_Casino,
            0 AS TO_Sport,
            0 AS ActiveDays,
            0 AS Retained_PrM,
            0 AS Retained_Bef_PrM,
            0 AS DepRet_PrM,
            0 AS DepRet_Bef_PrM,
            0 AS old_depositors
        FROM DWH.DM_CUSTOMERS C
        JOIN DWH.DIM_BRANDS BR ON BR.ENTITY_IDT=C.ENTITY_IDT
        JOIN DWH.DIM_COUNTRIES DC ON DC.COUNTRY_IDT=C.COUNTRY_IDT
        WHERE C.type IN ('casino','sport') AND CAST(C.created AS DATE) BETWEEN '2024-01-01' AND '2024-12-31'
        GROUP BY
            BR.ENTITY_NAME,
            BR.License,
            BR.affiliate_program,
            C.domain,
            DC.COUNTRY_ISO,
            DC.COUNTRY_NAME,
            CASE WHEN LOWER(C.traffic)='affiliate' THEN 'affiliate' ELSE 'media' END,
            LAST_DAY(C.created)
        UNION
        SELECT
            BR.ENTITY_NAME,
            BR.License,
            BR.affiliate_program,
            C.domain,
            DC.COUNTRY_ISO,
            DC.COUNTRY_NAME,
            CASE WHEN LOWER(C.traffic)='affiliate' THEN 'affiliate' ELSE 'media' END AS traffic,
            LAST_DAY(C.first_deposit_date) AS mm,
            0 AS reg,
            COUNT(*) AS ftd,
            COUNT(CASE WHEN LOWER(DCE.product_preference) IN ('mix','pure_sport') THEN 1 ELSE NULL END) AS ftd_sport,
            0 AS deposit,
            0 AS depositors,
            0 AS deposit_FTD,
            0 AS withdraw,
            0 AS withdraw_FTD,
            0 AS Actives,
            0 AS Actives_sport,
            0 AS GGRCasino_EUR,
            0 AS bonus_GGRCasino_EUR,
            0 AS GGRSport_EUR,
            0 AS bonus_GGRSport_EUR,
            0 AS DepositCount,
            0 AS NGR,
            0 AS NGR_FTD,
            0 AS active_VIP,
            0 AS FTD_VIP,
            0 AS dep_VIP,
            0 AS net_VIP,
            0 AS NGR_VIP,
            0 AS TO_Casino,
            0 AS TO_Sport,
            0 AS ActiveDays,
            0 AS Retained_PrM,
            0 AS Retained_Bef_PrM,
            0 AS DepRet_PrM,
            0 AS DepRet_Bef_PrM,
            0 AS old_depositors
        FROM DWH.DM_CUSTOMERS C
        JOIN DWH.DIM_BRANDS BR ON BR.ENTITY_IDT=C.ENTITY_IDT
        JOIN DWH.DIM_COUNTRIES DC ON DC.COUNTRY_IDT=C.COUNTRY_IDT
        JOIN DWH.DM_CUSTOMERS_EXTRA DCE ON C.ACCOUNT_IDT=DCE.ACCOUNT_IDT
        WHERE C.type IN ('casino','sport') AND C.first_deposit_date BETWEEN '2023-01-01' AND '2024-12-31'
        GROUP BY
            BR.ENTITY_NAME,
            BR.License,
            BR.affiliate_program,
            C.domain,
            DC.COUNTRY_ISO,
            DC.COUNTRY_NAME,
            CASE WHEN LOWER(C.traffic)='affiliate' THEN 'affiliate' ELSE 'media' END,
            LAST_DAY(C.first_deposit_date)
        UNION
        SELECT
            BR.ENTITY_NAME,
            BR.License,
            BR.affiliate_program,
            C.domain,
            DC.COUNTRY_ISO,
            DC.COUNTRY_NAME,
            CASE WHEN LOWER(C.traffic)='affiliate' THEN 'affiliate' ELSE 'media' END AS traffic,
            LAST_DAY(Reporting_Date) AS mm,
            0 AS reg,
            0 AS ftd,
            0 AS ftd_sport,
            SUM(Deposit_Amount_EUR) AS deposit,
            COUNT(DISTINCT CASE WHEN CC.Deposit_Count>0 THEN CC.account_id ELSE NULL END) AS depositors,
            SUM(CASE WHEN LAST_DAY(C.first_deposit_date) = LAST_DAY(Reporting_Date) THEN CC.Deposit_Amount_EUR ELSE 0 END) AS deposit_FTD,
            SUM(CC.Withdraw_Amount_EUR) AS withdraw,
            SUM(CASE WHEN LAST_DAY(C.first_deposit_date) = LAST_DAY(Reporting_Date) THEN CC.Withdraw_Amount_EUR ELSE 0 END) AS withdraw_FTD,
            COUNT(DISTINCT CASE WHEN CC.Turnover_Casino_EUR+CC.Turnover_Sport_EUR>0 THEN CC.account_id ELSE NULL END) AS Actives,
            COUNT(DISTINCT CASE WHEN CC.Turnover_Sport_EUR>0 THEN CC.account_id ELSE NULL END) AS Actives_sport,
            SUM(GGR_Casino_EUR) AS GGR_Casino_EUR,
            SUM(bonus_GGR_Casino_EUR) AS bonus_GGR_Casino_EUR,
            SUM(GGR_Sport_EUR) AS GGRSport_EUR,
            SUM(bonus_GGR_Sport_EUR) AS bonus_GGRSport_EUR,
            SUM(Deposit_Count) AS DepositCount,
            SUM(NGR) AS NGR,
            SUM(CASE WHEN LAST_DAY(C.first_deposit_date) = LAST_DAY(Reporting_Date) THEN NGR ELSE 0 END) AS NGR_FTD,
            COUNT(DISTINCT CASE WHEN CC.Turnover_Casino_EUR+CC.Turnover_Sport_EUR>0 AND NVL(L.LEVEL,1)>2 THEN CC.account_id ELSE NULL END) AS active_VIP,
            COUNT(DISTINCT CASE WHEN LAST_DAY(C.first_deposit_date) = LAST_DAY(Reporting_Date) AND NVL(L.LEVEL,1)>2 THEN CC.account_id ELSE NULL END) AS FTD_VIP,
            SUM(CASE WHEN NVL(L.level,1)>2 THEN CC.Deposit_Amount_EUR ELSE 0 END) AS dep_VIP,
            SUM(CASE WHEN NVL(L.level,1)>2 THEN CC.Deposit_Amount_EUR-CC.Withdraw_Amount_EUR ELSE 0 END) AS net_VIP,
            SUM(CASE WHEN NVL(L.level,1)>2 THEN CC.NGR ELSE 0 END) AS NGR_VIP,
            SUM(CC.Turnover_Casino_EUR+CC.bonus_Turnover_Casino_EUR) AS TO_Casino,
            SUM(CC.Turnover_Sport_EUR+CC.bonus_Turnover_Sport_EUR) AS TO_Sport,
            COUNT(CASE WHEN CC.Turnover_Casino_EUR+CC.Turnover_Sport_EUR>0 THEN CONCAT(C.account_id,C.domain) ELSE NULL END) AS ActiveDays,
            COUNT(DISTINCT CASE WHEN CC.Turnover_Casino_EUR+CC.Turnover_Sport_EUR>0 AND LAST_DAY(C.first_deposit_date) = dateadd('month', -1, date_trunc('month',Reporting_Date)) THEN CC.account_id ELSE NULL END) AS Retained_PrM,
            COUNT(DISTINCT CASE WHEN CC.Turnover_Casino_EUR+CC.Turnover_Sport_EUR>0 AND LAST_DAY(C.first_deposit_date) < dateadd('month', -1, date_trunc('month',Reporting_Date))THEN CC.account_id ELSE NULL END) AS Retained_Bef_PrM,
            COUNT(DISTINCT CASE WHEN CC.Deposit_Count>0 AND LAST_DAY(C.first_deposit_date) = dateadd('month', -1, date_trunc('month',Reporting_Date)) THEN CC.account_id ELSE NULL END) AS DepRet_PrM,
            COUNT(DISTINCT CASE WHEN CC.Deposit_Count>0 AND LAST_DAY(C.first_deposit_date) < dateadd('month', -1, date_trunc('month',Reporting_Date)) THEN CC.account_id ELSE NULL END) AS DepRet_Bef_PrM
           ,0 --,            COUNT(DISTINCT CASE WHEN CC.Deposit_Count>0 AND LAST_DAY(D.Last_Dep) = LAST_DAY(ReportingDate,-1) AND LAST_DAY(C.first_deposit_date) < LAST_DAY(ReportingDate,-1) THEN CC.account_id ELSE NULL END) AS old_depositors
        FROM DWH.DM_CUSTOMER_DAILY_ACTIVITY CC
        JOIN DWH.DM_CUSTOMERS C ON CC.ACCOUNT_IDT=C.ACCOUNT_IDT
        JOIN DWH.DIM_BRANDS BR ON BR.ENTITY_IDT=C.ENTITY_IDT
        JOIN DWH.DIM_COUNTRIES DC ON DC.COUNTRY_IDT=C.COUNTRY_IDT
        LEFT JOIN MVL AS L ON C.ACCOUNT_IDT=L.ACCOUNT_IDT AND L.LAST_LEVEL=1
           LEFT JOIN (
                SELECT MAX(A.REPORTING_DATE) AS LastDep
                FROM DWH.DM_CUSTOMER_DAILY_ACTIVITY A
            ) D
        WHERE C.type IN ('casino','sport') AND CC.Reporting_Date BETWEEN '2024-01-01' AND '2024-12-31'
        GROUP BY
            BR.ENTITY_NAME,
            BR.License,
            BR.affiliate_program,
            C.domain,
            DC.COUNTRY_ISO,
            DC.COUNTRY_NAME,
            CASE WHEN LOWER(C.traffic)='affiliate' THEN 'affiliate' ELSE 'media' END,
            LAST_DAY(Reporting_Date)
    ) k
    JOIN DWH.DIM_BRANDS br ON br.ENTITY_NAME = k.ENTITY_NAME AND br.domain = k.domain
    WHERE br.business IN ('B2C') AND LOWER(k.traffic) IN ('affiliate', 'media')
    GROUP BY
        k.mm,
        br.business,
        k.License,
        k.affiliate_program,
        k.ENTITY_NAME,
        k.traffic,
        CASE
            WHEN k.country_iso IN ('DE','IT','FI','SE','PL','NO','AT','IE','HU','CA','CZ','BR','CH','IN','NZ','JP','TH','AU','EE','TR','CL','PE','SK','SI','KZ','AZ') THEN k.country_iso
            WHEN k.country_name IN ('ROW1','ROW2','ROW3') THEN k.country_name
            WHEN k.country_iso='DK' AND k.License = 'DGA' THEN k.country_iso
            WHEN k.country_iso IN ('KW','BH','QA','AE','SA','LB','JO','OM','IQ','YE','EG') THEN 'ME'
            ELSE 'ROW'
        END
;
